<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = [
        'user_id', 'responsible', 'name', 'cnpj', 'email', 'mobile_number', 'radio_px', 'cep', 'address', 'number', 'complement', 'city', 'state', 'associates', 'link', 'file', 'user_id_intern'
    ];

    public function scopeCurrentUser($query)
	{
		if(Auth::user()->role_id == 2) {
	    	return $query->where('user_id_intern', Auth::user()->id);
	    }
	}
}
