<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use File;
use Image;
use Storage;
use FFMpeg;
use FFMpeg\Filters\Video\VideoFilters;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Publication;
use App\Club;
use App\Dealer;

class WsController extends Controller
{
    private $user;
    private $publication;
    private $club;
    private $dealer;

    public function __construct(
        User $user,
        Publication $publication,
        Club $club,
        Dealer $dealer
    )
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->club = $club;
        $this->dealer = $dealer;
    }

    //Login / geração de token
    public function loginApi(Request $request)
    {

        if (Auth::attempt(['cpf' => $request['cpf'], 'password' => $request['password']])) {
            return Auth::user();
        }
        else
        {
            $insert = $this->user->create([
                'user_id' => $request['user_id'],
                'name' => $request['name'],
                'email' => $request['email'],
                'cpf' => $request['cpf'],
                'password' => Hash::make($request['password']),
            ]);

            if($insert)
            {
                return response(200);
            }
            else
            {
                return response(400);
            }
        }
    }

    //Insere nova publicação em Mundo Troller
    public function newPublication(Request $request)
    {
        $month = 'November';
        $year = '2020';

        if($request['type'] == 'map')
        {
            $insert = $this->publication->create([
                'user_id' => $request['user_id'],
                'type' => $request['type'],
                'title' => $request['title'],
                'text' => $request['text'],
                'region' => $request['region'],
                'session' => $request['session'],
            ]);
        }
        else if($request['type'] == 'article')
        {
            $insert = $this->publication->create([
                'user_id' => $request['user_id'],
                'type' => $request['type'],
                'title' => $request['title'],
                'text' => $request['text'],
                'session' => $request['session'],
            ]);
        }
        else if($request['type'] == 'audio' || $request['type'] == 'photo' || $request['type'] == 'video')
        {
            $name = explode('.', $request['file']);

            if($request['type'] == 'audio')
            {
                $file = $name[0] . '.mp3';
            }

            if($request['type'] == 'video')
            {
                $file = $name[0] . 'ok.mp4';
            }

            if($request['type'] == 'photo')
            {
                $file = $request['file'];
            }

            $insert = $this->publication->create([
                'user_id' => $request['user_id'],
                'type' => $request['type'],
                'title' => $request['title'],
                'session' => $request['session'],
                'file' => 'publications/' . $month . $year . '/' . $file
            ]);
        }

        if($insert)
        {
            return response(200);
        }
        else
        {
            return response(400);
        }
    }

    //Upload de imagem, vídeo ou audio de Mundo Troller
    public function uploadFile(Request $request)
    {
        $file      = $request->file('media0');
        // dd($file);
        $filename  = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $name = explode('.', $filename);

        $month = 'November';
        $year = '2020';

        if($extension == 'amr' || $extension == 'wav' || $extension == 'aac') {
            if($file->move(storage_path('app/public/publications/' . $month . $year . '/'), $filename))
            {
                FFMpeg::fromDisk('publications')
                ->open($filename)
                ->export()
                ->toDisk('publications')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($name[0].'.mp3');

                Storage::delete('public/publications/' . $month . $year . '/' .$filename);
            }

            // Storage::disk('local')->put('public/publications/'. $month . $year . '/' . $filename, file_get_contents($file));

        }
        if($extension == 'mp4' || $extension == 'MOV') {

            if($file->move(storage_path('app/public/publications/' . $month . $year . '/'), $filename))
            {
                FFMpeg::fromDisk('publications')
                ->open($filename)
                ->export()
                ->toDisk('publications')
                ->inFormat(new FFMpeg\Format\Video\X264('libmp3lame', 'libx264'))
                ->addFilter(function (VideoFilters $filters) {
                    $filters->resize(new \FFMpeg\Coordinate\Dimension(480, 480));
                })
                ->save($name[0].'ok.mp4');

                Storage::delete('public/publications/' . $month . $year . '/' .$filename);

                // $fileNameFinal = $name[0].'ok.mp4';
            }

            // Storage::disk('local')->put('public/publications/'. $month . $year . '/' . $filename, file_get_contents($file));
        }
        if ($extension == 'jpg')
        {

            $img = Image::make($file)->resize(1000, 1000, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->orientate()->encode($extension, 80);

            Storage::disk('local')->put('public/publications/'. $month . $year . '/' . $filename, (string)$img);
        }
    }

    public function uploadAvatar(Request $request)
    {
        $file      = $request->file('media0');
        // dd($file);
        $filename  = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $name = explode('.', $filename);

        $month = 'November';
        $year = '2020';

        if ($extension == 'jpg')
        {

            $img = Image::make($file)->fit(800, 800, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->orientate()->encode($extension, 80);

            Storage::disk('local')->put('public/users/'. $month . $year . '/' . $filename, (string)$img);

            $update = $this->user
                ->where('user_id', $request['userId'])
                ->update([
                    'avatar' => 'users/' . $month . $year . '/' . $filename,
                ]);

            if($update)
            {
                return 'users/' . $month . $year . '/' . $filename;
            }
            else
            {
                return response(400);
            }
        }
    }

    //Retorna publicações gerais da timeline do Mundo Troller
    public function getPublications($session=null, $userid=null) {

        if($session == "null") {
            $session = null;
        }

        if($userid == "null") {
            $userid = null;
        }

        $arrPublications = array();

        $publications = DB::table('publications')
                    ->leftJoin('users as user', 'user.user_id', '=', 'publications.user_id')
                    //quando existir $session
                     ->when($session, function ($query) use ($session) {
                            return $query->where('session', '=', $session);
                      })
                     //quando existir $userid
                     ->when($userid, function ($query) use ($userid) {
                            return $query->where('publications.user_id', '=', $userid);
                      })
                    ->where('status', '=', 1)
                    ->orderBy('publications.id', 'DESC')
                    ->get([
                        'user.id as publicationUserIdInt', //interno
                        'user.user_id as publicationUserId',
                        'user.name as publicationUser',
                        'user.avatar as userAvatar',
                        'publications.id as publicationId',
                        'publications.type as publicationType',
                        'publications.title as publicationTitle',
                        'publications.text as publicationText',
                        'publications.file as publicationFile',
                        'publications.region as publicationRegion',
                        'publications.session as publicationSession',
                        'publications.created_at as publicationDate',
                            ]);

        foreach($publications as $publication)
        {
            if($publication->publicationFile == null)
            {
                $publicationFile = null;
            }
            else {
                $file = explode('"', $publication->publicationFile);

                if(count($file)>1) 
                {
                    $fileok = stripslashes($file[3]);
                }
                else {
                    $fileok = $publication->publicationFile;
                }

                $publicationFile = env('APP_URL') . '/storage/' . $fileok;
            }

            if($publication->publicationSession == 'dealer')
            {
                $dealer = \App\Dealer::where('user_id', $publication->publicationUserId)->first();

                $publicationUser = $dealer->name;
                $publicationSessionId = $dealer->id;
            }
            else if($publication->publicationSession == 'club')
            {
                $club = \App\Club::where('user_id', $publication->publicationUserId)->first();

                $publicationUser = $club->name;
                $publicationSessionId = $club->id;
            }
            else {
                $publicationUser = $publication->publicationUser;
                $publicationSessionId = $publication->publicationUserIdInt;
            }

            

            $arrPublications[] = [
                'publicationUserId' => $publication->publicationUserId,
                'publicationUser' => $publicationUser,
                'publicationId' => $publication->publicationId,
                'publicationType' => $publication->publicationType,
                'publicationTitle' => $publication->publicationTitle,
                'publicationText' => $publication->publicationText,
                'publicationRegion' => $publication->publicationRegion,
                'publicationFile' => $publicationFile,
                'publicationDate' => $publication->publicationDate,
                'publicationSession' => $publication->publicationSession,
                'publicationSessionId' => $publicationSessionId,
                'userAvatar' => $publication->userAvatar,
            ];

            
        }

        return $arrPublications;
    }

    //Retorna lista de benefícios
    public function getBenefits($type=null) {

        if($type == "null") {
            $type = null;
        }

        $arrBenefits = array();

        $benefits = DB::table('benefits')
                    ->leftJoin('benefit_types as benefit_type', 'benefit_type.id', '=', 'benefits.type_id')
                     
                     //quando existir $type
                     ->when($type, function ($query) use ($type) {
                            return $query->where('type_id', '=', $type);
                      })
                     ->orderBy('benefits.id', 'DESC')
                     ->get([
                        'benefits.id as benefitId',
                        'benefit_type.title as benefitType',
                        'benefits.title as benefitTitle',
                        'benefits.text as benefitText',
                        'benefits.link as benefitLink',
                        'benefits.file as benefitFile',
                            ]);

        foreach($benefits as $benefit)
        {
            $arrBenefits[] = [
                'benefitId' => $benefit->benefitId,
                'benefitType' => $benefit->benefitType,
                'benefitTitle' => $benefit->benefitTitle,
                'benefitText' => $benefit->benefitText,
                'benefitLink' => $benefit->benefitLink,
                'benefitFile' => env('APP_URL') . '/storage/' . $benefit->benefitFile
            ];
        }

        return $arrBenefits;
    }

    public function getBenefitTypes() {
        $types = \App\BenefitType::all();
        return $types->toArray();
    }

    public function getBanners() {
        $banners = \App\Banner::all();
        $bannerImages = array();
        
        foreach($banners as $banner) {
            $bannerImg = env('APP_URL') . '/storage/' . $banner->image;

            $bannerImages[] = [
                'bannerImg' => $bannerImg,
                'bannerLink' => $banner->link,
            ];
        }

        return $bannerImages;
    }

    //Insere novo Clube
    public function newClub(Request $request)
    {
        $insert = $this->club->create([
            'user_id_intern' => $request['user_id_intern'],
            'user_id' => $request['user_id'],
            'responsible' => $request['responsible'],
            'name' => $request['name'],
            'cnpj' => $request['cnpj'],
            'email' => $request['email'],
            'mobile_number' => $request['mobile_number'],
            'radio_px' => $request['radio_px'],
            'cep' => $request['cep'],
            'address' => $request['address'],
            'number' => $request['number'],
            'complement' => $request['complement'],
            'city' => $request['city'],
            'state' => $request['state'],
            'associates' => $request['associates'],
            'link' => $request['link'],
        ]);

        if($insert)
        {
            return response(200);
        }
        else
        {
            return response(400);
        }
    }

    //Altera os dados de um Clube específico
    public function updateClub(Request $request)
    {
        $update = $this->club
            ->where('user_id', $request['user_id'])
            ->update([
                'responsible' => $request['responsible'],
                'name' => $request['name'],
                'cnpj' => $request['cnpj'],
                'email' => $request['email'],
                'mobile_number' => $request['mobile_number'],
                'radio_px' => $request['radio_px'],
                'cep' => $request['cep'],
                'address' => $request['address'],
                'number' => $request['number'],
                'complement' => $request['complement'],
                'city' => $request['city'],
                'state' => $request['state'],
                'associates' => $request['associates'],
                'link' => $request['link'],
            ]);


        if($update)
        {
            return response(200);
        }
        else
        {
            return response(400);
        }
    }

    //Retorna dados de um determinado Clube do usuário
    public function getClub($user_id) {
        $club = \App\Club::where('user_id', $user_id)->get();
        return $club->toArray();
    }

    //Retorna dados de um usuário
    public function getUser($user_id) {
        $user = \App\User::where('user_id', $user_id)->get();
        return $user->toArray();
    }

    //Retorna lista de Clubes de todos os usuários ou de apenas um usuário se $id estiver setado
    public function getClubs($id=null) {

        $arrClubs = array();

        $clubs = DB::table('clubs')
                    ->when($id, function ($query) use ($id) {
                            return $query->where('id', '=', $id);
                    })
                    ->where('status', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->get();

        foreach($clubs as $club)
        {
            $arrClubs[] = [
                'clubId' => $club->id,
                'userId' => $club->user_id,
                'clubResponsible' => $club->responsible,
                'clubName' => $club->name,
                'clubCnpj' => $club->cnpj,
                'clubEmail' => $club->email,
                'clubMobileNumber' => $club->mobile_number,
                'clubRadioPx' => $club->radio_px,
                'clubCep' => $club->cep,
                'clubAddress' => $club->address,
                'clubNumber' => $club->number,
                'clubComplement' => $club->complement,
                'clubCity' => $club->city,
                'clubState' => $club->state,
                'clubAssociates' => $club->associates,
                'clubLink' => $club->link,
                'clubStatus' => $club->status,
                // 'clubFile' => env('APP_URL') . '/storage/' .$club->file
            ];
        }

        return $arrClubs;
    }

    //Retorna o status de um Clube, 0 = inativo, 1 = ativo
    public function checkClub($user_id) {
        $club = DB::table('clubs')
                    ->where('user_id', '=', $user_id)
                    ->first();
        if($club) {
            return $club->status;
        }
        else {
            return 'null';
        }
    }

    //Insere nova Concessionária do usuário
    public function updateDealer(Request $request)
    {
        $update = $this->dealer
            ->where('user_id', $request['user_id'])
            ->update([
                'name' => $request['name'],
                'cnpj' => $request['cnpj'],
                'email' => $request['email'],
                'phone_number' => $request['phone_number'],
                'cep' => $request['cep'],
                'address' => $request['address'],
                'number' => $request['number'],
                'complement' => $request['complement'],
                'city' => $request['city'],
                'state' => $request['state'],
                'opening_hours' => $request['opening_hours'],
                'link' => $request['link'],
            ]);


        if($update)
        {
            return response(200);
        }
        else
        {
            return response(400);
        }
    }

    //Retorna os dados da Concessionária do usuário
    public function getDealer($user_id) {
        $dealer = \App\Dealer::where('user_id', $user_id)->get();
        return $dealer->toArray();
    }

    //Retorna o status de uma Concessionária, 0 = inativo, 1 = ativo
    public function checkDealer($user_id) {
        $club = DB::table('dealers')
                    ->where('user_id', '=', $user_id)
                    ->first();
        if($club) {
            return $club->status;
        }
        else {
            return null;
        }
    }

    //Retorna os Mapas cadastrados
    public function getMaps($region=null) {

        if($region == "null") {
            $region = null;
        }


        $arrMaps = array();

        $maps = DB::table('publications')
                    ->leftJoin('users as user', 'user.user_id', '=', 'publications.user_id')
                    //quando existir $region
                     ->when($region, function ($query) use ($region) {
                            return $query->where('region', '=', $region);
                      })
                     ->where('publications.type', '=', 'map')
                     ->orderBy('publications.id', 'DESC')
                     ->get([
                        'user.id as mapUserIdInt', //interno
                        'user.user_id as mapUserId',
                        'user.name as mapUser',
                        'publications.id as mapId',
                        'publications.title as mapTitle',
                        'publications.region as mapRegion',
                        'publications.text as mapText',
                        'publications.created_at as mapDate'
                            ]);

        foreach($maps as $map)
        {
            $arrMaps[] = [
                'mapUser' => $map->mapUser,
                'mapId' => $map->mapId,
                'mapTitle' => $map->mapTitle,
                'mapRegion' => $map->mapRegion,
                'mapText' => $map->mapText,
                'mapDate' => $map->mapDate,
            ];
        }

        return $arrMaps;
    }

    //Retorna os textos das Políticas e Sobre o App
    public function getTexts($type) {
        $text = \App\Text::where('type', $type)->get();
        return Response::json($text);
    }
}
