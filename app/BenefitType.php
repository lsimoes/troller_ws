<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitType extends Model
{
    protected $fillable = [
        'title', 'type_id'
    ];
}
