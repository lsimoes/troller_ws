<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    protected $fillable = [
        'user_id', 'name', 'cnpj', 'email', 'phone_number', 'cep', 'address', 'number', 'complement', 'city', 'state', 'opening_hours', 'link', 'file', 'user_id_intern'
    ];

    public function scopeCurrentUser($query)
	{
		if(Auth::user()->role_id == 2) {
	    	return $query->where('user_id_intern', Auth::user()->id);
	    }
	}
}
