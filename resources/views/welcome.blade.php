<!DOCTYPE html>
<html lang="pt-br">

<head>

	<!-- Page meta -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Aplicativo Mundo Troller" />
	<meta name="keywords" content="#" />
	<meta name="author" content="#" />

	<!-- Favicons -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">

	<!-- Page Title -->
	<title>Mundo Troller</title>

	<!-- CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/animate.min.css" />
	<link rel="stylesheet" href="fonts/fontawesome/css/all.css" />
	<link rel="stylesheet" href="css/iconfont.css" />
	<link rel="stylesheet" href="css/magnific-popup.css" />
	<link rel="stylesheet" href="css/tipso.min.css" />
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/padding-margin.css" />

</head>

<body class="content-animate">

	
	<!-- PAGE
	==================================================-->
	<div id="top" class="page">

		

		<!-- Main Content
		==================================================-->
		<main class="cd-main-content">

			<!-- HOME SECTION --- before-section = rasgado--- 
			================================================== -->
			<section class="split-home page-section clearfic">
			
			
			<div class='split-pane col-xs-12 col-sm-6 parallax-2' class="pl-0" data-background="images/logo.png">
            </div>
			
			
				<div class='split-pane col-xs-12 col-sm-6 heading-30 text-center'>
					<div>
					

                      
                        <ul  class="pl-0">
					<h5 class="fw500 text-center underline">Aplicativo Mundo Troller</h5><hr>
                    <a href="#"><li class="white-color"><span>Baixe o app para Android</span></li></a>
                    <a href="#"><li class="white-color"><span>Baixe o app para iOS</span></li></a>
                    <a href="https://mundotroller.com.br/storage/Termos-MundoTroller.pdf"><li class="white-color"><span>Termos de uso</span></li></a>
                    <a href="https://mundotroller.com.br/storage/Politica-MundoTroller.pdf"><li class="white-color"><span>Política de privacidade</span></li></a>
                    <a href="https://mundotroller.com.br/admin"><li class="white-color"><span>Admin --</span></li></a>
					</ul>
					</div>
										
				</div>

				
			</section>
			
			


		</main>

	</div>

	<!-- JAVASCRIPT
	==================================================-->
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.localScroll.min.js"></script>
	<script src="js/jquery.viewport.mini.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.fitvids.js"></script>
	<script src="js/equalize.min.js"></script>
	<script src="js/jquery.parallax-1.1.3.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/isotope.pkgd.min.js"></script>
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<script src="js/masonry.pkgd.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/tipso.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/swiper.min.js"></script>
	<script src="js/home-init.js"></script>
	<script src="js/text-slider.js"></script>

</body>

</html>