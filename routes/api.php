<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['middleware' => 'auth:api'], function() {

// });

Route::post('/login-api','WsController@loginApi');
Route::post('/upload-file','WsController@uploadFile');
Route::post('/upload-avatar','WsController@uploadAvatar');
Route::get('/getTexts/{type}','WsController@getTexts');

Route::group(['middleware' => 'client'], function() {
    Route::post('/newPublication','WsController@newPublication');
    Route::get('/getPublications/{session?}/{userid?}','WsController@getPublications');
    Route::get('/getBenefits/{type?}','WsController@getBenefits');
    Route::get('/getBenefitTypes','WsController@getBenefitTypes');
    Route::get('/getClubs/{id?}','WsController@getClubs');
    Route::get('/getClub/{user_id?}','WsController@getClub');
    Route::get('/checkClub/{user_id?}','WsController@checkClub');
    Route::get('/getMaps/{region?}','WsController@getMaps');
    Route::post('/newClub','WsController@newClub');
    Route::post('/updateClub','WsController@updateClub');
    Route::get('/checkDealer/{user_id?}','WsController@checkDealer');
    Route::get('/getDealer/{user_id?}','WsController@getDealer');
    Route::post('/updateDealer','WsController@updateDealer');
    Route::get('/getBanners','WsController@getBanners');
    Route::get('/getUser/{user_id?}','WsController@getUser');
});
