<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('responsible');
            $table->string('name');
            $table->string('cnpj');
            $table->string('email');
            $table->string('mobile_number');
            $table->string('radio_px');
            $table->string('cep');
            $table->string('address');
            $table->integer('number');
            $table->string('complement');
            $table->string('city');
            $table->string('state');
            $table->string('associates');
            $table->string('link');
            $table->string('file')->nullable();
            $table->integer('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
