<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->enum('type', ['map', 'article', 'audio', 'photo', 'video']);
            $table->string('title');
            $table->text('text')->nullable();
            $table->enum('region', ['norte', 'nordeste', 'centrooeste', 'sudeste'])->nullable();
            $table->string('file')->nullable();
            $table->string('session'); //user, club, dealer
            $table->integer('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
